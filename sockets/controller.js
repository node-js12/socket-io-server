require('colors')

const socketControl = socket => {

    console.log("client connected".green, socket.id)
    socket.on('disconnect', () => {
        console.log("client disconnected".red)
    })
    // chat => este nombre es el mismo que se le da en el front
    socket.on('chat', (payload, callback) => {
        const id = 12345;
        // si recibe payload envia de regreso el id
        callback(id)
        console.log("mensaje client ".blue , payload)
        // broadcast => send mensaje all users
        socket.broadcast.emit('chat', payload)
    })
}

module.exports = socketControl