const socket = io()
// references HTML
let fragment = document.createDocumentFragment();
// templates
const receivedMessage = document.getElementById('receivedMessage').content
const sendMessage = document.getElementById('sendMessage').content

const container_messages = document.getElementById('container-messages')

const online = document.querySelector('.text-success')
const disconnect = document.querySelector('.text-danger')
const btnSend = document.getElementById('btnSend')
const msgClient = document.getElementById('msgClient')

// on => escuchar eventos
socket.on('connect', () => {
    console.log('conectado')
    disconnect.classList.add('d-none')
    online.classList.remove('d-none')
 })

socket.on('disconnect',() => {
    console.log('disconnect!')
    online.classList.add('d-none')
    disconnect.classList.remove('d-none')
})

// event listener send messagge
btnSend.addEventListener('click',() => {
    const payload = {
        msg: msgClient.value,
        id: 12,
        date: new Date()
    }

    // send messagge
    socket.emit('chat', payload, (id) => {
        console.log("desde el servidor ", id)
        mostrarSendMsg(payload)
    })
    
    msgClient.value = ''
})

// receive message from server
socket.on('chat', payload => {
    console.log("response: ", payload)
    mostrarMsgReceived(payload)
})

const mostrarMsgReceived = payload => {
    receivedMessage.querySelector('.receivedMessage').textContent = payload.msg
    const clone = receivedMessage.cloneNode(true)
    fragment.appendChild(clone)
    container_messages.appendChild(fragment)
}

const mostrarSendMsg = payload => {
    console.log(payload.msg)
    sendMessage.querySelector('.sendMsg').textContent = payload.msg
    const clone = sendMessage.cloneNode(true)
    fragment.appendChild(clone)
    container_messages.appendChild(fragment)
    console.log(fragment)

}