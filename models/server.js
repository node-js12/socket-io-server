const express = require('express')
const cors = require('cors')
const socketControl = require('../sockets/controller')


class Server {
    
    constructor() {
        this.port = process.env.PORT
        this.app = express()
        this.server = require('http').createServer(this.app);
        // io, servidor de los sockets
        this.io = require('socket.io')(this.server);
        this.paths = {}
        // middlewares => añaden funcionalidad a mi app
        this.middlewares()
        // routes app.
        this.routes()
        // sockets configuracion
        this.sockets();
    }

    middlewares() {

        // cors
        this.app.use(cors())

        // use => inicialize middleware (/)
        this.app.use(express.static('public'))

    }

    routes() {
        // this.app.use(this.paths.auth, require('../routes/auth'))  
    }
    sockets(){
        this.io.on('connection', socketControl )
    }

    listen() {
        this.server.listen(this.port, () => {
            console.log('Corriendo en el puerto.....', this.port)
        })
    }


}

module.exports = Server;